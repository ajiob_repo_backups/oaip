/*
	Ввести m и n. Определить, являются ли они взаимнопростыми.
*/
#include <stdio.h>
#include <conio.h>
#include <locale.h>
int main()
{
	system("chcp 65001");		//Русификация

	int m,n;

	printf("Введите целые m и n\n");
	scanf_s("%d%d",&m,&n);				//Ввод

	if (m==n)
	{
		printf("Числа не являются взаимнопростыми");
		_getch();
		return 0;
	}									//m==n + return


	if (m<n)
	{
		int mn;
		mn=m;
		m=n;
		n=mn;
	}									//m>n на выходе

	for (int i=2;i<=n;i++)
	{
		if (m%i==0 && n%i==0)
		{
			printf("Числа не являются взаимнопростыми");
			_getch();
			return 0;
		}
	}									//цикл проверки на делимость

	printf("Числа взаимнопросты");

	_getch();
	return 0;
}