/*
	Является ли n! произведением 3 последовательных натуральных чисел
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
int main()
{
	system("chcp 65001");

	int n, x;
	double n_fact=1;				//n!

	printf("Введите натуральное число\n");
	scanf("%d", &n);

	for(int i=2; i<=n; i++)				//находит n!
	{
		n_fact*=i;
	}

	x=pow(n_fact, 1./3)+1;				//х - единственно возможное среднее число. Если не оно - тогда никакое

	if (x*x*x-x==n_fact)
	{
		printf("%d!=%d*%d*%d\n",n,x-1,x,x+1);
	}
	else
	{
		printf("%d! не является произведением трех последовательных натуральных чисел\n",n);
	}

	fflush(stdin);
	system("pause");
	return 0;
}