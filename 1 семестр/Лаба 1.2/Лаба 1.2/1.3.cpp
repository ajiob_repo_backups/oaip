/*
	Вычислить значение функции для фиксированного числа итераций или пока приращение не будет меньше введенной с клавиатуры точности.
	f=1+x/4+1*5*x^2/4*8+...
*/

#include <stdio.h>
#include <iostream>
#include <locale.h>
#define n 5								//кол-во итераций
int main()
{
	system("chcp 65001");		//русификация

	double x, f=0,e;

	printf("Введите х\n");
	scanf("%lf", &x);					//ввод x

	printf("Введите точность\n");
	scanf("%d", &e);					//ввод e (точности)

	double k=1;
	for (int i=1; i<=n; i++)
	{
		k*=((4*i-3)*x/(4*i));			//подсчет (1*5*9*.../4*8*12*...)*x^n
		if (k>=e)
		{
			f+=k;
		}
		else break;						//проверка на точность
	}									//цикл подсчета f

	printf("%lf\n",f);

	system("pause");
	return 0;
}