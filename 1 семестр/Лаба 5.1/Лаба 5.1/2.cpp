/*
	Параметрами передать слова. Найти макс. из них и напечатать его
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>

void calc (int, char**);

int main(int kol, char **ms)
{
	system("chcp 65001");
	printf ("Здравствуйте! Вы запустили программу, которая принимает параметрами слова, находит самое длинное из них и печатает его.\n");

	calc(kol, ms);

	printf ("\nПрограмма завершена. Для выхода из программы введите любой символ.\n");
	fflush (stdin);
	getch();
	return 0;
}

void calc (int k, char** m)
{
	int len_max, len_now, i, i_max, j;
	for (i=1; i<k; i++)					//перебор каждого слова
	{
		len_now=0;						//обнуление текущей длины
		for (j=0; m[i][j]; j++)				//нахождение длины слова
		{
			len_now++;
		}

		if(i==1 || len_now>len_max)				//сохранение макс. значений
		{
			len_max=len_now;
			i_max=i;
		}
	}

	printf("\nСловом максимальной длины является слово №%d. Само слово:\n",i_max);				//вывод результатов
	puts(m[i_max]);
}