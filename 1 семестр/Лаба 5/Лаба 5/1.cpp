/*
	Функция находит массив с максимальной суммой и выводит его на экран.
	Функция принимает разное число параметров
	fun1: int**
	fun2: void**
	fun3: va_list, ...
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include <stdarg.h>

void fun1(int, ...);
void fun2(int, ...);
void fun3(int, ...);

int main()
{
	system("chcp 65001");
	printf ("Здравствуйте! Вы запустили программу, которая находит массив с максимальной суммой и выводит его на экран.\n");

	int ms1[]={1,3,5,6,9},
		ms2[]={2,6,9},
		ms3[]={9,0,-5},
		ms4[]={8,8,9,8,3,-6},
		ms5[]={-6,8,3};

	fun1(3, ms3, sizeof(ms3)/sizeof(int), ms5, sizeof(ms5)/sizeof(int), ms1, sizeof(ms1)/sizeof(int));
	fun2(3, ms2, sizeof(ms2)/sizeof(int), ms4, sizeof(ms4)/sizeof(int), ms1, sizeof(ms1)/sizeof(int));
	fun3(3, ms1, sizeof(ms1)/sizeof(int), ms4, sizeof(ms4)/sizeof(int), ms2, sizeof(ms2)/sizeof(int));

	printf ("\nПрограмма завершена. Для выхода из программы введите любой символ.\n");
	fflush (stdin);
	getch();
	return 0;
}

void fun1(int k, ...)					//через int **
{
	int **m;			//хранит передаваемые массивы
	m=(int **)&k;		//запомнили позицию k
	m++;				//теперь он стоит на позиции первого элемента массива

	int *m_leng;			//хранит длины массивов
	m_leng=&k;				//запомнили позицию k
	m_leng+=2;				//теперь он стоит на позиции длины первого элемента

	printf("\nФункция 1\n");

	if (k<=0)
	{
		printf("Было передано, что ничего не будет передано\n");
		return;
	}

	int sum_max, sum_now, i_max, i, j;

	for (i=0; i<k; i++)				//перебирает каждую строку
	{
		sum_now=0;						//обнуляем текущую сумму
		for (j=0; j<*(m_leng+2*i); j++)					//находит сумму этого массива
		{
			sum_now+=*(*(m+2*i)+j);
		}

		if (i==0 || sum_now>sum_max)				//запоминает сумму для старта или max сумму
		{
			sum_max=sum_now;
			i_max=i;
		}
	}

	printf("Переданный массив №%d имеет максимальную сумму, которая равна %d.\nЭтот массив:\n", i_max+1, sum_max);
	for (j=0; j< *(m_leng+2*i_max); j++)
	{
		printf("%d   ", *(*(m+2*i_max)+j));
	}
}

void fun2(int k, ...)					//через void**
{
	void **m;			//хранит передаваемые массивы
	m=(void **)&k;		//запомнили позицию k
	m=(void **)(((int **)m)+1);				//теперь он стоит на позиции первого элемента массива

	void *m_leng;			//хранит длины массивов
	m_leng=(void *)&k;				//запомнили позицию k
	m_leng=(void *)((int *)m_leng+2);				//теперь он стоит на позиции длины первого элемента

	printf("\nФункция 2\n");

	if (k<=0)
	{
		printf("Было передано, что ничего не будет передано\n");
		return;
	}

	int sum_max, sum_now, i_max, i, j;

	for (i=0; i<k; i++)				//перебирает каждую строку
	{
		sum_now=0;						//обнуляем текущую сумму
		for (j=0; j<*((int *)m_leng+2*i); j++)					//находит сумму этого массива
		{
			sum_now+=*(*((int **)m+2*i)+j);
		}

		if (i==0 || sum_now>sum_max)				//запоминает сумму для старта или max сумму
		{
			sum_max=sum_now;
			i_max=i;
		}
	}

	printf("Переданный массив №%d имеет максимальную сумму, которая равна %d.\nЭтот массив:\n", i_max+1, sum_max);
	for (j=0; j< *((int *)m_leng+2*i_max); j++)
	{
		printf("%d   ", *(*((int **)m+2*i_max)+j));
	}
}

void fun3(int k, ...)				//через макросы
{
	va_list(m);			//хранит передаваемые массивы
	va_start(m, k);		//запомнили позицию элемента после k

	int *m_leng;			//хранит длины массивов
	m_leng=&k;				//запомнили позицию k
	m_leng+=2;				//теперь он стоит на позиции длины первого элемента

	printf("\nФункция 3\n");

	if (k<=0)
	{
		printf("Было передано, что ничего не будет передано\n");
		return;
	}

	int sum_max, sum_now, i_max, i, j,* tmp,* tmp_max;

	for (i=0; i<k; i++)				//перебирает каждую строку
	{
		sum_now=0;						//обнуляем текущую сумму
		tmp=(int *)(va_arg(m, int *));					//сохраняем адрес массива для работы
		va_arg(m, int);
		for (j=0; j<*(m_leng+2*i); j++)					//находит сумму этого массива
		{
			sum_now+=*(tmp+j);
		}

		if (i==0 || sum_now>sum_max)				//запоминает сумму для старта или max сумму
		{
			sum_max=sum_now;
			i_max=i;
			tmp_max=tmp;
		}
	}

	va_end(m);

	printf("Переданный массив №%d имеет максимальную сумму, которая равна %d.\nЭтот массив:\n", i_max+1, sum_max);
	for (j=0; j< *(m_leng+2*i_max); j++)
	{
		printf("%d   ", *(tmp_max+j));
	}
}