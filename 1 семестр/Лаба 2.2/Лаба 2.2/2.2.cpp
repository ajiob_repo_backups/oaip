/*
	Перевести число из десятичной системы исчисления в римскую.
	В римской системе счисления I обозначает 1, V обозначает 5, X - 10, L - 50, C - 100, D - 500, M - 1000.
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int main()
{
	system("chcp 65001");

	int n;

	printf("Введите натуральное число, меньшее 4000\n");
	for(;;)
	{
		scanf_s("%d",&n);
		if (n>0 && n<4000)
		{
			break;
		}
		printf("Число введено неправильно, повторите ввод\n");
	}									//проверяет правильность введенного числа

	printf("Число в римском представлении: ");

	for(;n>=1000;n-=1000)
	{
		printf("M");
	}									//проверяет тысячи

	switch (n/100)
	{
		case 9: printf("CM"); n-=900; break;
		case 8: printf("DCCC"); n-=800; break;
		case 7: printf("DCC"); n-=700; break;
		case 6: printf("DC"); n-=600; break;
		case 5: printf("D"); n-=500; break;
		case 4: printf("CD"); n-=400; break;
		case 3: printf("CCC"); n-=300; break;
		case 2: printf("CC"); n-=200; break;
		case 1: printf("C"); n-=100; break;
	}									// проверяет сотни

	switch (n/10)
	{
		case 9: printf("XC"); n-=90; break;
		case 8: printf("LXXX"); n-=80; break;
		case 7: printf("LXX"); n-=70; break;
		case 6: printf("LX"); n-=60; break;
		case 5: printf("L"); n-=50; break;
		case 4: printf("XL"); n-=40; break;
		case 3: printf("XXX"); n-=30; break;
		case 2: printf("XX"); n-=20; break;
		case 1: printf("X"); n-=10; break;
	}									// проверяет десятки

	switch (n)
	{
		case 9: printf("IX"); break;
		case 8: printf("VIII"); break;
		case 7: printf("VII"); break;
		case 6: printf("VI"); break;
		case 5: printf("V"); break;
		case 4: printf("IV"); break;
		case 3: printf("III"); break;
		case 2: printf("II"); break;
		case 1: printf("I"); break;
	}									//проверяет единицы

	printf("\n");
	system("pause");
	return 0;
}