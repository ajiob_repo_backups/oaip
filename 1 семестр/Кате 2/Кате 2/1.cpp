#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>

long int fun(int a1, int d, int n)
{
	if (n<=1)
	{
		return a1;
	}
	return d+fun(a1, d, n-1);
}

int main()
{
	system("chcp 65001");
	printf ("Здравствуйте! Вы запустили программу, которая посчитает n-ый член арифметичесокй прогрессии.\n");

	int n, a1, d;
	int rez;

	printf("Введите первый член прогрессии\n");
	scanf("%d", &a1);

	printf("Введите разность прогрессии\n");
	scanf("%d", &d);

	printf("Введите номер члена, который требуется вычислить\n");
	scanf("%d", &n);

	rez=fun(a1, d, n);

	printf("%d-ый член прогрессии равен %d", n, rez);

	printf ("\nПрограмма завершена. Для выхода из программы введите любой символ.\n");
	fflush (stdin);
	getch();
	return 0;
}