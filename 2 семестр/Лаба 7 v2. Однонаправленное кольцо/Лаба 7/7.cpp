/*
	Есть файл с текстовой информацией (a-z + 0-9)
	Однонаправленная очередь
	1.1) закачать файл в кольцо
	1.2) отображение кольца
	2) оставить в кольце только цифры
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>

struct ring
{
	char info;
	ring *next;
};

//для работы с кольцом (заносит и удаляет следующий за list элемент)
void push(ring **list, char to_add)
{
	ring *tmp;

	tmp=(ring*)malloc(1*sizeof(ring));								//выделение памяти
	if (!tmp) return;

	tmp->info=to_add;														//заполнение полей элемента
	if (!(*list))															//если кольцо пустое, замыкаем его на себя
	{
		*list=tmp->next=tmp;
		return;
	}
	tmp->next=(*list)->next;
	(*list)->next=tmp;
	*list=tmp;																//т.к. точка входа в кольцо - это "последний элемент"
}
char pop(ring **list)
{
	if(!(*list)) return 0;														//кольцо пусто
	ring *tmp=(*list)->next;
	char c=tmp->info;																//то, что хранится внутри

	if (tmp->next==tmp)																//если в кольце один элемент
	{
		*list=nullptr;
	}
	else
	{
		(*list)->next=(*list)->next->next;
	}

	free(tmp);																		//удаление старой верхушки
	return c;
}
char see_top(ring *list)
{
	if(!list) return 0;
	return list->next->info;
}
void see(ring *list)
{
	if(!list)
	{
		printf("Кольцо пусто\n");
		return;
	}

	ring *tmp=list->next;																				//для того, чтобы гулять по кольцу

	printf("---------------То, что в кольце-------------\n");
	do
	{
		printf("%c\n", tmp->info);
		tmp=tmp->next;
	}
	while(tmp!=list->next);
	printf("----------------Конец--------------\n");
	return;
}

//Скопировать файл в кольцо
void cpy_file_into_ring(const char f_adress[], ring** list)
{
	FILE* f=fopen(f_adress,"rb");
	if (!f)
	{
		printf("Файл не создан\n");
		return;
	}

	char c;

	while(1)
	{
		fscanf(f,"%c",&c);									//читаем текущий символ
		if (feof(f))										//если дошли до конца файла
		{
			break;
		}
		push(list,c);										//добавляем в кольцо
	}

	fclose(f);
}

//Оставить в кольце только цифры
void del_ring_element_if_not_num(ring** list)
{
	if(!(*list))
	{
		printf("Кольцо пусто\n");
		return;
	}

	ring *tmp=*list;																				//для того, чтобы гулять по кольцу

	do
	{
		if(see_top(tmp)<'0' || see_top(tmp)>'9')														//если не цифра - тогда удалить этот элемент
		{
			pop(&tmp);
			continue;
		}

		tmp=tmp->next;
	} while ((tmp->next)!=(*list));

	if(see_top(tmp)<'0' || see_top(tmp)>'9')														//если не цифра - тогда удалить этот элемент
	{
		pop(&tmp);
		*list=tmp;																						//двигаем вершину, если элемент нужно удалить
	}

	return;
}

//текст меню
void menu_text(int key=0)						//текст меню
{
	printf("\nВыберите то, что вы хотите сделать:\n");					//текст меню
	printf("1. Скопировать файл в кольцо\n");
	printf("2. Вывести кольцо на экран\n");
	printf("3. Оставить в кольце только цифры\n");
	/*
	printf("4. Удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину стека следующего элемента очереди\n");
	printf("5. Удалить человека\n");
	printf("6. Добавить человека\n");
	*/
	printf("0. Выход\n");
	printf("Пожалуйста, введите цифру\n");
}
//само меню
int menu()
{
	ring *begin=nullptr;											//точка входа в кольцо
	char f_adress[]="info.bin";
	int k;				//для меню
	do
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					return 0;
				}
			case 1:
				{											//Скопировать файл в кольцо
					cpy_file_into_ring(f_adress, &begin);
					break;
				}
			case 2:
				{											//Вывести кольцо на экран
					see(begin);
					break;
				}
			case 3:
				{											//Оставить в кольце только цифры
					del_ring_element_if_not_num(&begin);
					break;
				}/*
			case 4:
				{											//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
					del_min_q(&head, &tail);
					printf("\n");
					see_q(head);							//простотр того, что в стеке
					break;
				}
			case 5:											//удалить структуру
				{
					delpeople(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 6:											//добавить структуру
				{
					add_to_fix_pos(file_adress);
					break;
				}*/
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать кольцом.\n");

	menu();

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}
