/*
	вводятся цифры с клавиатуры в список
	удалить элементы, нарушающие упорядоченность (по возрастанию)
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>

struct queue
{
	int info;
	queue* next;
};

int setint()
{
	int n;												//вводимое число

	printf("Введите число\n");
	do
	{
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите целое число\n");
	} while(1);

	return n;
}

//для работы с очередью
void push_q(queue **head, queue **tail, int info_to_add)										//добавление элемента очереди
{
	queue *tmp;

	tmp=(queue*)malloc(1*sizeof(queue));								//выделение памяти
	if (!tmp) return;

	tmp->info=info_to_add;														//заполнение полей элемента
	tmp->next=nullptr;

	if(*head)															//если очередь пуста, нужно записать и голову, и хвост
	{
		(*tail)->next=tmp;
		*tail=tmp;
		return;
	}

	*head=*tail=tmp;															//изменение хвоста очереди
}
int pop_q(queue **head, queue **tail)
{
	if(!(*head)) return 0;
	queue *tmp=*head;
	int to_return=(*head)->info;																//то, что хранится внутри

	*head=(*head)->next;															//изменение головы очереди

	if(!(*head))																	//если удален единственный элемент, тогда хвост указывает на nullptr
	{
		*tail=nullptr;
	}

	free(tmp);																		//удаление старой головы очереди
	return to_return;
}
int see_q_top(queue *head)
{
	if(!head) return 0;
	return head->info;
}
void see_q(queue *head)
{
	if(!head)
	{
		printf("Очередь пуста\n");
		return;
	}

	printf("---------------То, что в очереди-------------\n");
	while(head)
	{
		printf("%d\n", head->info);
		head=head->next;
	}
	printf("----------------Конец--------------\n");
	return;
}

//ввод очереди
void enter_q(queue **head, queue **tail)
{
	int tmp;
	do
	{
		tmp=setint();														//ввод числа
		push_q(head, tail,tmp);														//заначение его в очередь

		printf("Если вы хотите ввести еще одно число, введите '+'\n");
		fflush(stdin);
	} while (getchar()=='+');
}

//удаляет следующий за next_will_be_deleted элемент
int pop_any_q_element(queue **head, queue **tail, queue *next_will_be_deleted)
{
	if (!next_will_be_deleted)
	{
		return 0;
	}

	queue *tmp=next_will_be_deleted->next;

	int to_return=next_will_be_deleted->next->info;													//то, что возвращаем

	next_will_be_deleted->next=next_will_be_deleted->next->next;										//перебрасываем связь

	if ( (*tail)==(next_will_be_deleted->next))														//если удаляется последний элемент
	{
		*tail=next_will_be_deleted;
	}

	free(tmp);
	return to_return;
}

//удаление элементов, нарушающих упорядоченность
void del_element_if_not_sotred(struct queue **head, struct queue **tail)
{
	if (!(*head))												//если очередь пуста - выход
	{
		printf("Очередь пуста\n");
		return;
	}

	if ( !((*head)->next) )												//если в очереди один элемент - выход
	{
		return;
	}

	queue* tmp;
	for (tmp=*head; (tmp->next);)									//гуляем по циклу и ищем лишние (нарушающие упорядоченность) элементы
	{
		if ( (tmp->info)>(tmp->next->info) )											//элемент нарушает упорядоченность
		{
			pop_any_q_element(head, tail, tmp);												//удаляем его
			continue;
		}

		tmp=tmp->next;
	}
}

//очистка очереди
void del_all_q(queue **head, queue **tail)
{
	while ( *head)
	{
		pop_q(head, tail);
	}
}

//что мы выбрали в меню
int menu_to_switch()
{
	int for_return=0;
	int max_switch=4;
	char enter_now;
	//char *words[]="Hello";

	char *words[]={
		"1. Создать (дозаписать) очередь",
		"2. Вывести очередь на экран",
		"3. Удалить элементы, нарушающие упорядоченность по возрастанию",
		"4. Очистить очередь",
		"0. Выход"
	};

	while(1)
	{
		system("cls");
		printf("Выберите то, что вы хотите сделать:\n");
		for (int i=0; i<=max_switch; i++)
		{
			printf("%s", words[i]);
			if (i==for_return)
			{
				printf("        <--");
			}
			printf("\n");
		}
		printf("Пожалуйста, введите цифру\n");

		fflush(stdin);
		enter_now=getch();

		if (enter_now=='W' || enter_now=='w')									//идем вверх по списку
		{
			for_return--;
			if (for_return<0)
			{
				for_return=max_switch;									//максимальное из возможных меню
			}
		}
		else if (enter_now=='S' || enter_now=='s')									//идем вниз по списку
		{
			for_return++;
			if (for_return>max_switch)
			{
				for_return=0;									//минимальное из возможных меню
			}
		}
		else if (enter_now=='\n' || enter_now=='\r')									//возвращаем то, что выбрано
		{
			return (for_return+1)%(max_switch+1);
		}
		else return enter_now-'0';														//возвращаем то, что ввели
	}
}
//само меню
int menu()
{
	queue *head=nullptr, *tail=nullptr;											//точка входа в кольцо
	int k;				//для меню
	do
	{
		k=menu_to_switch();

		switch(k)
		{
			case 0:										//выход
				{
					return 0;
				}
			case 1:
				{											//Ввести очередь
					enter_q(&head, &tail);
					fflush(stdin);
					getch();
					break;
				}
			case 2:
				{											//Вывести очередь на экран
					see_q(head);
					fflush(stdin);
					getch();
					break;
				}
			case 3:
				{											//Удалить элементы, нарушающие упорядоченность по возрастанию
					del_element_if_not_sotred(&head, &tail);
					printf("\n");
					see_q(head);							//просмотр того, что в стеке
					fflush(stdin);
					getch();
					break;
				}
			case 4:
				{											//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
					del_all_q(&head, &tail);
					printf("\n");
					see_q(head);							//просмотр того, что в стеке
					fflush(stdin);
					getch();
					break;
				}
				/*
			case 5:											//удалить структуру
				{
					delpeople(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 6:											//добавить структуру
				{
					add_to_fix_pos(file_adress);
					break;
				}*/
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					fflush(stdin);
					getch();
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая удалит из очереди элементы, нарушающие упорядоченность.\n");

	menu();

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}