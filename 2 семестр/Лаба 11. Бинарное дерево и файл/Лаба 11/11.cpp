/*
	в файле хранятся слова (имена)
	составить дерево имен
	дерево: ключ - длина слова
	при попытке занести имеющуюся длину - не заносить просто
	в дереве найти самое длинное имя
	удалить его из файла
*/
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <io.h>

struct tree
{
	char* info;
	tree* left;
	tree* right;
};

//для работы с деревом (добавление элементов)
void push(tree **root, char* info_to_add)										//добавление элемента дерева
{
	if (*root)																	//ищем позицию для сохранения
	{
		if (strlen((*root)->info) > strlen(info_to_add))
		{
			push(&((*root)->left), info_to_add);
			return;
		}
		else if(strlen((*root)->info) == strlen(info_to_add))
		{
			return;
		}
		else
		{
			push(&((*root)->right), info_to_add);
			return;
		}
	}

	tree *tmp;
	char *s=(char *)malloc((strlen(info_to_add)+1)*sizeof(char));
	if (!s) return;
	strcpy(s, info_to_add);

	tmp=(tree*)malloc(1*sizeof(tree));								//выделение памяти
	if (!tmp)
	{
		free(s);
		return;
	}

	tmp->info=s;														//заполнение полей элемента
	tmp->left=tmp->right=nullptr;

	*root=tmp;															//изменение хвоста очереди
}

tree* delete_element(tree **root, char* info_to_delete)										//поиск элемента дерева
{
	if (!root)
	{
		return nullptr;
	}

	if (strlen((*root)->info) > strlen(info_to_delete))
	{
		return delete_element(&((*root)->left), info_to_delete);
	}

	if (strlen((*root)->info) < strlen(info_to_delete))
	{
		return delete_element(&((*root)->right), info_to_delete);
	}

	tree *tmp, *for_return=*root;
	*root=tmp=(*root)->left;

	if (!tmp)																			//левого элемента нету
	{
		*root=for_return->right;															//в корень стал правый элемент
		return for_return;
	}

	while (tmp->right)																	//ищем место для корня правой ветки
	{
		tmp=tmp->right;
	}

	tmp->right=for_return->right;														//цепляем правую ветку к левой

	return for_return;
}
//рекурсивный вывод содержимого бинарного дерева
void see(tree *root)
{
	if(!root)
	{
		return;
	}

	printf("Element: %s\n", root->info);									//будем выводить в отсортированном виде

	if (root->left) see(root->left);										// вывод левой ветви дерева

	if (root->right) see(root->right);										// вывод правой ветви дерева
}

tree* pop_min_element(tree **root)										//добавление элемента дерева
{
	if(!(*root))
	{
		return nullptr;
	}

	if((*root)->left)
	{
		return pop_min_element(&((*root)->left));
	}

	tree *to_return=*root;											//минимальный элемент

	*root=(*root)->right;											//изменяем корень

	to_return->right=nullptr;

	return to_return;
}

tree* pop_max_element(tree **root)										//добавление элемента дерева
{
	if(!(*root))
	{
		return nullptr;
	}

	if((*root)->right)
	{
		return pop_max_element(&((*root)->right));
	}

	tree *to_return=*root;											//минимальный элемент

	*root=(*root)->left;											//изменяем корень

	to_return->left=nullptr;

	return to_return;
}

int calculate_elements(tree *root)										//добавление элемента дерева
{
	if(!root)
	{
		return 0;
	}

	return calculate_elements(root->left)+calculate_elements(root->right)+1;
}

void balanse_analysis_and_change_left(tree** root, int delta)
{
	if(delta<=0) return;

	tree* tmp_root=*root, *tmp_now=nullptr;
	while(delta)
	{
		tmp_now = pop_max_element(&((*root)->left));																	//возвращаем максимальный элемент
		tmp_now->right = tmp_root;
		tmp_root = tmp_now;																					//теперь tmp_root - вершина временного дерева (линейного)
		delta--;
	}

	tmp_root->left = (*root)->left;																	//переприкрепляем елвую ветку

	(*root)->left = nullptr;																		//убираем старую привязку

	*root = tmp_root;																				//переопределяем корень
}

void balanse_analysis_and_change_right(tree** root, int delta)
{
	if(delta<=0) return;

	tree* tmp_root=nullptr, *tmp_now=nullptr;
	while(delta)
	{
		tmp_now = pop_min_element(&((*root)->right));																	//возвращаем минимальный элемент
		tmp_now->left = tmp_root;
		tmp_root = tmp_now;																					//теперь tmp_root - вершина временного дерева (линейного)
		delta--;
	}

	tmp_root->right = (*root)->right;																	//переприкрепляем елвую ветку

	(*root)->right = nullptr;																		//убираем старую привязку

	*root = tmp_root;																				//переопределяем корень
}
//дельта=левая-правая ветки
void balanse_analysis_and_change(tree** root, int delta)
{
	if (!delta)														//не нужно ничего балансировать - выход
	{
		return;
	}

	if (delta>0)
	{
		balanse_analysis_and_change_left(root, delta);
		return;
	}

	balanse_analysis_and_change_right(root, -delta);
}

void balanse_tree(tree **root)
{
	if(!(*root))							//дерево пустое - выход
	{
		return;
	}

	int num_left, num_right;													//количество элементов слева//количество элементов справа
	num_left = calculate_elements((*root)->left);
	num_right = calculate_elements((*root)->right);

	int how_much_we_need_to_transfer;
	how_much_we_need_to_transfer = (num_left - num_right) / 2;									//сколько элементов нужно перебросить с одной ветки в другую

	balanse_analysis_and_change(root, how_much_we_need_to_transfer);

	balanse_tree(&((*root)->left));
	balanse_tree(&((*root)->right));
}

//читаем из файла в дерево
void read_file_into_tree(char f_adress[], tree **root)
{
	char *string_now;
	int i;
	FILE* f=fopen(f_adress, "rb");
	if (!f)
	{
		printf("Cannot open file\n");
		return;
	}

	string_now=(char *)malloc((filelength(fileno(f))+1)*sizeof(char));
	if(!string_now)
	{
		printf("Нужно больше памяти\n");
		return;
	}

	while (!feof(f))
	{
		fscanf(f, "%s", string_now);											//читаем слово

		push(root, string_now);													//добавляем символ в дерево
	}

	fclose(f);
}

//функция ввода строки
char* setst()
{
	int n, i;												//длина слова//для цикла
	char* s;											//вводимое слово

	do
	{
		printf("Введите максимальную длину строки\n");
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите число\n");
	} while(1);

	n++;
	s=(char*)malloc(n*sizeof(char));
	if (!s) return nullptr;

	printf("Введите строку\n");
	fflush(stdin);
	fgets(s,n,stdin);

	for(i=0; s[i] && s[i]!='\n'; i++);							//теперь в конце гарантированно \0
	s[i]='\0';

	return s;
}

//функции удаления участка файла
void del_smth_from_file(char adress[], fpos_t begin, fpos_t end)
{
	if (begin>=end) return;										//если начало дальше конца - то выйти
	FILE* f=fopen(adress,"r+b");								//открываем файл для изменения
	if (!f)
	{
		printf("Cannot open file\n");
		return;
	}

	char tmp;													//в нее будем писать по символу

	for(; ; begin+=sizeof(char), end+=sizeof(char))							//цикл переброса участка файла
	{
		fsetpos(f, &end);										//ставим УТПФ на число, которое будем забирать
		fread(&tmp, sizeof(char), 1, f);									//забираем число
		if (feof(f)) break;										//если достигли конца файла - выход
		fsetpos(f, &begin);										//ставим УТПФ на число, в которое будем писать
		fwrite(&tmp, sizeof(char), 1, f);									//пишем число
	}

	chsize(fileno(f), begin);

	fclose(f);													//закрытие файла
}

//удаляем из файла строку
void delete_string_from_file(char f_adress[], char* string_to_delete)
{
	char* string_now;
	FILE* f=fopen(f_adress, "rb");
	if (!f)
	{
		printf("Cannot open file\n");
		return;
	}

	string_now=(char *)malloc((filelength(fileno(f))+1)*sizeof(char));
	if(!string_now)
	{
		printf("Нужно больше памяти\n");
		return;
	}

	fpos_t begin_pos_in_file=0;														//позиция в файле для удаления
	fpos_t end_pos_in_file=0;														//позиция в файле для удаления

	while (!feof(f))
	{
		fgetpos(f, &begin_pos_in_file);																						//текущая позиция в файле
		fscanf(f, "%s", string_now);									//читаем строку

		if (!strcmp(string_now, string_to_delete))															//нашли совпадение - удаляем элемент
		{
			fgetpos(f, &end_pos_in_file);																						//текущая позиция в файле
			fclose(f);

			del_smth_from_file(f_adress, begin_pos_in_file, end_pos_in_file);

			return;
		}
	}

	fclose(f);
}

//Ищем элемент с максимальной длиной строки
tree* find_tree_element_with_max_kol(tree *root)
{
	if(!root)										//элемента нету
	{
		return root;
	}

	while (root->right)
	{
		root = root->right;
	}

	return root;
}

//пишем из строки в файл
void write_string_to_file(char f_adress[], char *s)
{
	FILE* f=fopen(f_adress, "wb");
	if (!f)
	{
		printf("Cannot open file\n");
		return;
	}

	fwrite(s, sizeof(char), strlen(s), f);

	fclose(f);
}

//вывод содержимого файла
void see_file(char *file_adress)
{
	FILE* f=fopen(file_adress, "rb");
	if (!f)
	{
		printf("Cannot open file\n");
		return;
	}

	char tmp;

	printf("\nСейчас в файле:\n");
	while(1)
	{
		fread(&tmp, sizeof(char), 1, f);
		if(feof(f)) return;
		putchar(tmp);
	}

	putchar('\n');

	fclose(f);
}


int main()
{
	system("chcp 65001");
	printf("Hello\n");

	char file_adress[]="bin_file.txt";
	tree *root=nullptr, *tmp;
	char *s="qwer qweroiwfa adkvl sdfkj ;las fsdvdskfdjsvjdsfkvnakjdf sdflksooa adfkvs";

	//s=setst();																		//ввод строки
	if (!s)
	{
		printf("Для продолжения недостаточно места");
		printf("Good bye\n");
		fflush(stdin);
		getch();
		return 0;
	}

	write_string_to_file(file_adress, s);

	read_file_into_tree(file_adress, &root);											//переписываем файл в бинарное дерево

	see(root);

	tmp=find_tree_element_with_max_kol(root);							//нашли требуемый элемент дерева

	if (tmp)																				//если дерево не пустое
	{
		printf("\nWe delete: %s\n", tmp->info);

		delete_string_from_file(file_adress, tmp->info);								//удалили все вхождения символа из файла

		delete_element(&root, tmp->info);												//удалили из дерева
	}
	else
	{
		printf("No elements for delete\n");
	}

	see_file(file_adress);

	printf("\nСейчас в дереве:\n");
	see(root);

	balanse_tree(&root);

	printf("\nСейчас в дереве:\n");
	see(root);

	printf("Good bye\n");
	fflush(stdin);
	getch();
	return 0;
}