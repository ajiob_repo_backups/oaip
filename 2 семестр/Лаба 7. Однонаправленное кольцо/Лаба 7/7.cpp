/*
	Есть файл с текстовой информацией (a-z + 0-9)
	Однонаправленная очередь
	1.1) закачать файл в кольцо
	1.2) отображение кольца
	2) оставить в кольце только цифры
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>

struct ring
{
	char info;
	ring *next;
};

//для работы с кольцом (заносит следующий за list элемент, удаляет точку входа в кольцо)
void push(ring **list, char to_add)
{
	ring *tmp;

	tmp=(ring*)malloc(1*sizeof(ring));								//выделение памяти
	if (!tmp) return;

	tmp->info=to_add;														//заполнение полей элемента
	if (!(*list))															//если кольцо пустое, замыкаем его на себя
	{
		*list=tmp->next=tmp;
		return;
	}
	tmp->next=(*list)->next;
	(*list)->next=tmp;
}
char pop(ring **list)
{
	ring *tmp=*list;
	if(!tmp) return 0;
	char c=tmp->info;																//то, что хранится внутри

	if (tmp->next==tmp)
	{
		*list=nullptr;
	}
	else
	{
		for (; (tmp->next) != (*list); tmp=tmp->next);												//ищем элемент перед точкой входа
		(*list)=tmp;																				//пока вершина стека - предыдущий элемент
		tmp=tmp->next;																				//tmp - элемент для удаления
		(*list)->next=(*list)->next->next;															//удаление текущего элемента
		(*list)=(*list)->next;																		//теперь вершина - следующий за удаляемым элемент
	}

	free(tmp);																		//удаление старой верхушки
	return c;
}
char see_top(ring *list)
{
	if(!list) return 0;
	return list->info;
}
void see(ring *begin)
{
	ring *tmp=begin;																				//для того, чтобы гулять по кольцу
	if(!tmp)
	{
		printf("Кольцо пусто\n");
		return;
	}

	printf("---------------То, что в кольце-------------\n");
	do
	{
		printf("%c\n", tmp->info);
		tmp=tmp->next;
	}
	while(tmp!=begin);
	printf("----------------Конец--------------\n");
	return;
}

//Скопировать файл в кольцо
void cpy_file_into_ring(const char f_adress[], ring** begin)
{
	FILE* f=fopen(f_adress,"rb");
	if (!f)
	{
		printf("Файл не создан\n");
		return;
	}

	char c;

	while(1)
	{
		fscanf(f,"%c",&c);									//читаем текущий символ
		if (feof(f))										//если дошли до конца файла
		{
			break;
		}
		push(begin,c);										//добавляем в кольцо
	}

	fclose(f);
}

//Оставить в кольце только цифры
void del_ring_element_if_not_num(ring** begin)
{
	ring *tmp=*begin;																				//для того, чтобы гулять по кольцу
	*begin=nullptr;																			//пока начала нету
	if(!tmp)
	{
		printf("Кольцо пусто\n");
		return;
	}

	do
	{
		if(!((tmp->info)>='0' && (tmp->info)<='9'))														//если не цифра - тогда удалить этот элемент
		{
			pop(&tmp);
			continue;
		}

		if (!(*begin))																				//если начала нет - поставим начало
		{
			*begin=tmp;
		}
		tmp=tmp->next;
	} while(tmp!=(*begin));

	return;
}

//текст меню
void menu_text(int key=0)						//текст меню
{
	printf("\nВыберите то, что вы хотите сделать:\n");					//текст меню
	printf("1. Скопировать файл в кольцо\n");
	printf("2. Вывести кольцо на экран\n");
	printf("3. Оставить в кольце только цифры\n");
	/*
	printf("4. Удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину стека следующего элемента очереди\n");
	printf("5. Удалить человека\n");
	printf("6. Добавить человека\n");
	*/
	printf("0. Выход\n");
	printf("Пожалуйста, введите цифру\n");
}
//само меню
int menu()
{
	ring *begin=nullptr;											//точка входа в кольцо
	char f_adress[]="info.bin";
	int k;				//для меню
	do
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					return 0;
				}
			case 1:
				{											//Скопировать файл в кольцо
					cpy_file_into_ring(f_adress, &begin);
					break;
				}
			case 2:
				{											//Вывести кольцо на экран
					see(begin);
					break;
				}
			case 3:
				{											//Оставить в кольце только цифры
					del_ring_element_if_not_num(&begin);
					break;
				}/*
			case 4:
				{											//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
					del_min_q(&head, &tail);
					printf("\n");
					see_q(head);							//простотр того, что в стеке
					break;
				}
			case 5:											//удалить структуру
				{
					delpeople(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 6:											//добавить структуру
				{
					add_to_fix_pos(file_adress);
					break;
				}*/
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать кольцом.\n");

	menu();

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}
