#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include <locale.h>
#define N 30
struct database
{
	char surname[N];		//фамилия
	int god_rogd;			//год рождения
	char type;			//s-студент, r-работник, p-пенсионер
	union						//для каждого типа человека
	{
		struct			//студент
		{
			char VYZ[N];		//ВУЗ
			int group;			//группа
			double sum;			//стипендия
		} s;
		struct			//работник
		{
			char workplace[N];			//предприятие
			int dolzn;			//должность (1,2,3)
			double sum;			//зарплата
		} r;
		struct			//пенсионер
		{
			double sum;				//пенсия
		} p;
	} un;
};

void setname(database* db)					//ввод фамилии с клавиатуры
{
	printf("Введите фамилию человека\n");
	fflush(stdin);
	fgets(db->surname, N, stdin);
}

void setyear(database* db)					//ввод года рождения с клавиатуры (1900<...<2017)
{
	int tmp;
	do
	{
		printf("Введите год рождения (от 1901 до 2016)\n");
		fflush(stdin);
		scanf("%d", &tmp);
		if (tmp>1900 && tmp<=2016)
		{
			db->god_rogd=tmp;
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

void settype(database* db)									//ввод типа человека с клавиатуры
{
	char tmp;
	do
	{
		printf("Введите тип человека (s-студент, r-работник, p-пенсионер)\n");
		fflush(stdin);
		scanf("%c", &tmp);
		if (tmp=='s' || tmp=='r' || tmp=='p')
		{
			db->type=tmp;
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

void setsum(database* db, char* s)					//ввод суммы (стипендия, пенсия, з/п) с клавиатуры
{
	double tmp;
	do
	{
		printf("Введите %s\n",s);
		fflush(stdin);
		scanf("%lf", &tmp);
		if (tmp>=0)										//если такая з/п существует, тогда сохраняем ее, иначе повторный ввод
		{
			switch(db->type)
			{
				case 's':
				{
					db->un.s.sum=tmp;
					break;
				}
				case 'r':
				{
					db->un.r.sum=tmp;
					break;
				}
				case 'p':
				{
					db->un.p.sum=tmp;
					break;
				}
			}
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

void setuni(database* db, char* s)					//ввод университета/места работы с клавиатуры
{
	printf("Введите %s\n", s);
	fflush(stdin);
	switch(db->type)
	{
		case 's':
		{
			fgets(db->un.s.VYZ, N, stdin);
			break;
		}
		case 'r':
		{
			fgets(db->un.r.workplace, N, stdin);
			break;
		}
	}
}

void setgroup(database* db, char* s)					//ввод ID должности/номер группы с клавиатуры
{
	int tmp;
	do
	{
		printf("Введите %s\n", s);
		fflush(stdin);
		scanf("%d", &tmp);
		if (tmp>0)
		{
			switch(db->type)
			{
				case 's':
				{
					db->un.s.group=tmp;
					break;
				}
				case 'r':
				{
					db->un.r.dolzn=tmp;
					break;
				}
			}
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

database* getdb(int n)			//кол-во вводимых структур
{
	printf("____________________Ввод___________________\n");
	int i;
	database *db;
	db=(database*)malloc(n*sizeof(database));			//выделение памяти под структуру
	if(!db) return db;

	for (i=0; i<n; i++)
	{
		printf("\nВводится человек №%d\n", i+1);

		setname(db+i);						//ввод фамилии
		setyear(db+i);						//ввод года рождения
		settype(db+i);						//ввод типа человека

		switch( (db+i)->type)
		{
			case 's':
			{
				setuni(db+i, "ВУЗ");				//ввод ВУЗа
				setgroup(db+i, "№ группы");				//ввод № группы
				setsum(db+i, "стипендию");						//ввод стипендии
				break;
			}
			case 'r':
			{
				setuni(db+i, "место работы");			//ввод места работы
				setgroup(db+i, "№ дожности");					//ввод № должности
				setsum(db+i, "зарплату");				//ввод зарплаты
				break;
			}
			case 'p':
			{
				setsum(db+i, "пенсию");				//ввод пенсии
				break;
			}
		}

		printf("\nЧеловек №%d введен\n", i+1);
	}

	printf("Вы ввели всех людей\n\n");
	return db;
}

void putdb(database *db, int n)
{
	printf("____________________Вывод___________________\n");
	for (int i=0; i<n; i++)
	{
		printf("----------------Человек №%d--------------------\n", i+1);

		printf("Фамилия человека: %s", (db+i)->surname);			//вывод фамилии
		printf("Год рождения: %d\n", (db+i)->god_rogd);				//вывод года рождения

		switch( (db+i)->type)
		{
			case 's':
			{
				printf("Тип человека: студент\n");			//вывод типа человека
				printf("ВУЗ: %s", (db+i)->un.s.VYZ);			//вывод ВУЗа
				printf("Группа: %d\n", (db+i)->un.s.group);			//вывод № группы
				printf("Стипендия: %lf\n", (db+i)->un.s.sum);			//вывод стипендии

				break;
			}
			case 'r':
			{
				printf("Тип человека: работник\n");			//вывод типа человека
				printf("Место работы: %s",(db+i)->un.r.workplace);			//вывод места работы
				printf("№ должности: %d\n", (db+i)->un.r.dolzn);				//вывод № должности
				printf("Зарплата: %lf\n", (db+i)->un.r.sum);			//вывод зарплаты

				break;
			}
			case 'p':
			{
				printf("Тип человека: пенсионер\n");			//вывод типа человека
				printf("Пенсия: %lf\n", (db+i)->un.p.sum);		//вывод пенсии

				break;
			}
		}
	}
	printf("---------------------Конец--------------------\n\n");
}

void menu_text(int key)						//текст меню
{
	printf("Выберите то, что вы хотите сделать:\n");					//текст меню
	printf("1. Ввод базы данных с клавиатуры\n");
	printf("2. Вывод базы на экран\n");
	printf("0. Выход\n");
	printf("Пожалуйста, введите цифру\n");
}

int menu(int key)
{
	database *db=nullptr;
	int k, n=0;				//для меню//ко-во элементов БД
	do
	{
		menu_text(key);

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					free(db);
					return 0;
				}
			case 1:
				{								//ввод БД с коавиатуры
					do
					{
						printf("Введите кол-во элементов базы данных, которые вы хотите ввести\n");
						scanf("%d", &n);
					}while(n<0);

					db=getdb(n);				//ввод массива структур
					if (!db)
					{
						printf("Ошибка выделения памяти. Действие не удалось\n");
						n=0;					//обнуление n на случай, если попросят вывод
					}

					break;
				}
			case 2:											//вывод того, что ввели, на экран
				{
					putdb(db,n);
					break;
				}
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать с базой данных.\n");

	menu(0);

	printf("\nДля выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}