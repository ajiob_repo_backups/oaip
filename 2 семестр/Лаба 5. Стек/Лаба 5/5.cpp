/*
	Дана строка. Удалить
	-1-ое
	-последнее
	-любое
	слово. Сделать это рекурсивно
	Всё на СТЕКЕ
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>

struct stack
{
	char info;
	stack *next;
};

int setstrlen()
{
	int n;												//вводимое число

	printf("Введите длину строки\n");
	do
	{
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите целое число\n");
	} while(1);

	return n;
}
char* setst()
{
	char *s=nullptr;
	int i, n=setstrlen();												//для цикла

	s=(char *)calloc(n,sizeof(char));
	if(!s)
	{
		printf("Недостаточно памяти для записи такой строки\n");
		return s;
	}

	printf("Введите строку\n");
	fflush(stdin);
	fgets(s,n,stdin);
	s[n]='\0';

	for(i=0; s[i] && s[i]!='\n'; i++);							//теперь в конце гарантированно \0
	s[i]='\0';
	return s;
}

void push(stack **list, char c)
{
	stack *tmp;

	tmp=(stack*)malloc(1*sizeof(stack));								//выделение памяти
	if (!tmp) return;

	tmp->info=c;														//заполнение полей элемента
	tmp->next=*list;

	*list=tmp;															//изменение верхушки стека
}
char pop(stack **list)
{
	stack *tmp=*list;
	if(!(*list)) return 0;
	char c=(*list)->info;																//то, что хранится внутри

	*list=(*list)->next;															//изменение верхушки стека

	free(tmp);																		//удаление старой верхушки
	return c;
}
char see(stack **list)
{
	if(!(*list)) return 0;
	return (*list)->info;
}

void del_first_word(char s[])
{
	stack *list=nullptr;
	int i, len=0;									//len по умолчанию хранит длину удаляемого слова, а затем новую длину строки

	for (i=0; s[i] && s[i]!=' '; i++);												//пропускаем перове слово
	i++;
	len=i;

	for (; s[i]; i++)												//запись в стек нужного кол-ва строки
	{
		push(&list,s[i]);
	}

	len=strlen(s)-len+1;													//в len новая длина строки
	s=(char *)realloc(s,len*sizeof(char));
	if (!s)																//не выделилась память - освобождаем стек
	{
		while (list)													//пока стек не пуст, извлекаем элемент
		{
			pop(&list);
		}
		return;
	}

	len--;
	s[len]='\0';														//записываем символ конца строки
	len--;
	while (list)
	{
		s[len]=pop(&list);
		len--;
	}
}

void del_word(char s[])
{
	stack *list=nullptr;
	int i, len=0, n=0;									//len по умолчанию хранит длину удаляемого слова, а затем новую длину строки

	printf("Введите номер удаляемого слова (начиная с 1)\n");								//ввод номера удаляемого слова
	scanf("%d", &n);
	n--;

	for (i=0; s[i]; i++)												//запись в стек нужного кол-ва слов строки
	{
		if(!n)																//дошли до пропускаемого слова
		{
			len=i;															//запоминаем начало слова
			while(s[i] && s[i]!=' ')										//пропускаем слово
			{
				i++;
			}
			n--;
			len=i-len;														//теперь есть длина слова
			continue;
		}

		push(&list,s[i]);														//заносим символ в стек

		if(s[i]==' ') n--;														//значит, пропустили одно слово
	}

	len=strlen(s)-len+1;													//в len новая длина строки
	s=(char *)realloc(s,len*sizeof(char));
	if (!s)																//не выделилась память - освобождаем стек
	{
		while (list)													//пока стек не пуст, извлекаем элемент
		{
			pop(&list);
		}
		return;
	}

	len--;
	s[len]='\0';														//записываем символ конца строки
	len--;
	while (list)
	{
		s[len]=pop(&list);
		len--;
	}
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать со строкой на примере стека.\n");

	char *s=setst();							//введение строки

	//del_first_word(s);
	del_word(s);

	printf("Результат:\n");
	puts(s);
	free(s);

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}
