/*
	общежитие:
	лифт - список (двунаправленный)
	этаж - кольцо
	на этаже комнаты
	по каждой комнате:
	кто там живет?
	по каждому человеку досье
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>
#include <math.h>

//delay(); - задержка в миллисекундах

#define Len 20
#define Max_person_in_room 4
#define Max_room_in_stage 20
#define Max_stages 10

enum sex_type
{
	null=0,
	men=1,
	women=2,
	in_change = 3
};

struct person_type
{
	char name[Len];												//имя
	char surname[Len];											//фамилия
	sex_type sex;												//пол
	int id_number;												//номер студенческого билета
	int group_number;											//номер группы
	double average_ball;										//средний балл
};

struct room_type
{
	int stage;													//номер этажа, на котором находится комната
	int number;													//номер комнаты
	sex_type sex;												//пол жителей комнаты
	person_type person[Max_person_in_room];						//люди в комнате
};

struct stage_type												//кольцо
{
	int stage_number;											//номер этажа
	room_type room;												//комната на этаже
	stage_type* next;											//следующая комната
	stage_type* prev;											//предыдущая комната
};

struct lift_type												//очередь
{
	stage_type* stage;											//этаж в общежитиии
	lift_type* next;											//следующий этаж
	lift_type* prev;											//предыдущий этаж
};

struct hostel_type
{
	lift_type* begin;
	lift_type* end;
};

//функция ввода неотрицательного целого числа
int set_int_above_0()
{
	int num_to_return=-1, num_now;												//вводимое число (меньше нуля - число не введено)

	fflush(stdin);

	do
	{
		num_now=getchar();																//сканируем введенный символ

		if (!num_now || num_now=='\r' || num_now=='\n')									//вводились только цифры и мы дошли до конца введенной строки
		{
			if (num_to_return<0)														//передана пустая строка - повторим ввод
			{
				continue;
			}

			break;
		}

		if (num_now<'0' || num_now>'9')													//ввели не цифру
		{
			std::cout<<("Ошибка! Введите целое неотрицательное число\n");
			num_to_return=-1;
			fflush(stdin);
			continue;
		}

		if (num_to_return<0)																//всё-таки ввели какую-то цифру
		{
			num_to_return=0;
		}

		num_to_return*=10;
		num_to_return+=num_now-'0';
	} while(1);

	return num_to_return;
}
//функция ввода неотрицательного дробного числа
double set_double_above_0()
{
	int num_now;
	double num_to_return=-1, fractional_power=-1;						//введенное число//степень дробной части (-1 - степень нулевая)

	fflush(stdin);

	do
	{
		num_now=getchar();																//сканируем введенный символ

		if (!num_now || num_now=='\r' || num_now=='\n')									//вводились только цифры и мы дошли до конца введенной строки
		{
			if (num_to_return<0)														//передана пустая строка - повторим ввод
			{
				continue;
			}

			break;
		}

		if ((num_now=='.' || num_now==',') && fractional_power<0)
		{
			fractional_power=0;
		}

		if (num_now<'0' || num_now>'9')													//ввели не цифру
		{
			std::cout<<("Ошибка! Введите целое неотрицательное число\n");
			num_to_return=-1;
			fractional_power=-1;
			fflush(stdin);
			continue;
		}

		if (num_to_return<0)																//всё-таки ввели какую-то цифру
		{
			num_to_return=0;
		}

		if (fractional_power>=0)																//всё-таки ввели какую-то цифру
		{
			fractional_power++;
		}

		num_to_return*=10;
		num_to_return+=num_now-'0';
	} while(1);

	return num_to_return*pow(10, (-1)*fractional_power);
}
//функция ввода строки
void setst(char *s, int len)
{
	int i;												//для цикла

	//std::cout<<("Введите фамилию\n");
	fflush(stdin);
	fgets(s, len, stdin);

	for (i = 0; s[i] && s[i] != '\n'; i++);							//теперь в конце гарантированно \0
	s[i] = '\0';
}

//количество жителей в комнате
int person_amount_in_room(const room_type &room)
{
	int i;
	int person_in_room = 0;

	for (i=0; i < (sizeof(room.person)/sizeof(person_type)); i++)									//подсчет количества жителей конкретной комнаты
	{
		if (room.person[i].sex)
		{
			person_in_room++;
		}
	}

	return person_in_room;
}

//отображение в консоль
void show_person(const person_type &person_to_show)
{
	if (!(person_to_show.sex))														//если студент не заполнен (там никто не живет)
	{
		return;
	}

	std::cout<<("----------Студент-----------\n");
	std::cout<<("Имя: %s\n", person_to_show.name);
	std::cout<<("Фамилия: %s\n", person_to_show.surname);

	if (person_to_show.sex==1)
	{
		std::cout<<("Пол: мужской\n");
	}
	else if (person_to_show.sex==2)
	{
		std::cout<<("Пол: женский\n");
	}
	else
	{
		std::cout<<("Пол: не определен\n");
	}

	std::cout<<("Номер студенческого билета: %d\n", person_to_show.id_number);
	std::cout<<("Номер группы: %d\n", person_to_show.group_number);
	std::cout<<("Средний балл: %lf\n", person_to_show.average_ball);
}
void show_room(const room_type &room_to_show)
{
	if (!(room_to_show.sex))												//если комната пуста
	{
		return;
	}

	std::cout<<("==================Комната №%d (%d из %d жителей)=================", room_to_show.number, person_amount_in_room(room_to_show), sizeof(room_to_show.person)/sizeof(person_type));

	for (int i = 0; i < (sizeof(room_to_show.person)/sizeof(person_type)); i++)
	{
		show_person(room_to_show.person[i]);
	}
}
void show_stage(const stage_type* stage_to_show)
{
	stage_type* now = (stage_type*)stage_to_show;

	std::cout<<("+++++++++++++++Этаж №%d+++++++++++++++", stage_to_show->stage_number);

	do
	{
		show_room(now->room);
		now = now->next;
	}
	while(now != stage_to_show);
}
void show_all_hostel(const hostel_type &hostel_to_show)
{
	lift_type* now = hostel_to_show.begin;

	while(now)
	{
		show_stage(now->stage);
		now = now->next;
	}
}

//начальная инициализация общежития (конструктор)
hostel_type new_hostel()
{
	hostel_type to_return;
	to_return.begin = to_return.end = nullptr;
	return to_return;
}

//connect функции (связь механизмов и типа интерфейса)
void connect_show_room(const hostel_type &hostel)
{
	int room_number;										//номер комнаты для просмотра
	int i;													//для циклов
	stage_type* stage_now = nullptr;						//этаж (для поиска)
	room_type room_now;										//комната (для поиска и просмотра)
	room_now.sex = null;

	std::cout<<("Введите номер комнаты\n");
	room_number = set_int_above_0();

	//

	if (room_now.sex)
	{
		show_room(room_now);
	}
	else
	{
		std::cout<<("Такой комнаты не существует или она пустая\n");
	}
}
void connect_show_stage(const hostel_type &hostel)
{
	//
}

//текст меню
void menu_text(int key=0)						//текст меню
{

	if (key==1)
	{
		std::cout<<("\nВыберите то, что вы хотите просмотреть:\n");					//текст меню
		std::cout<<("1. Комнату\n");
		std::cout<<("2. Этаж\n");
		std::cout<<("3. Всё общежитие\n");

		std::cout<<("0. Назад\n");
		std::cout<<("Пожалуйста, введите цифру\n");
		return;
	}
	std::cout<<("\nВыберите то, что вы хотите сделать:\n");					//текст меню
	std::cout<<("1. Просмотреть что-либо\n");
	std::cout<<("2. Найти номер комнаты человека\n");
	//std::cout<<("3. Просмотреть \n");
	//std::cout<<("4. Удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину стека следующего элемента очереди\n");
	/*
	std::cout<<("5. Удалить человека\n");
	std::cout<<("6. Сохранить данные в файл\n");
	*/
	std::cout<<("0. Выход\n");
	std::cout<<("Пожалуйста, введите цифру\n");
}

//управляющая функция просмотра
void show(const hostel_type hostel_to_show)
{
	int k;																//для меню
	while(1)
	{
		menu_text(1);

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
			{
				return;
			}
			case 1:
			{											//просмотр комнаты
				connect_show_room(hostel_to_show);
				break;
			}
			case 2:
			{											//просмотр этажа
				connect_show_stage(hostel_to_show);
				break;
			}
			case 3:
			{											//просмотр общежития
				show_all_hostel(hostel_to_show);
				break;
			}
			default:
			{
				std::cout<<("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
				break;
			}
		}
	}
}

//само меню
int menu()
{
	hostel_type hostel = new_hostel();									//общежитие
	int k;				//для меню
	while(1)
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
			{
				return 0;
			}
			case 1:
			{											//Создать очередь стеков
				show(hostel);
				break;
			}
			case 2:
			{											//Вывести очередь на экран
				//see_q(head);
				break;
			}
			case 3:
			{											//Найти минимальный стек в очереди по сумме
				//find_min_sum(head);
				break;
			}
			case 4:
			{											//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
				//del_min_q(&head, &tail);
				std::cout<<("\n");
				//see_q(head);							//простотр того, что в стеке
				break;
			}/*
			case 5:											//удалить структуру
			{
				delpeople(file_adress);
				fprint(file_adress);						//то, что сейчас в файле
				break;
			}
			case 6:											//добавить структуру
			{
				add_to_fix_pos(file_adress);
				break;
			}*/
			default:
			{
				std::cout<<("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
				break;
			}
		}
	}
}

int main()
{
	system("chcp 65001");
	std::cout<<("Здравствуйте! Вы запустили программу, которая позволяет работать с общежитием.\n");

	menu();

	std::cout<<("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}




/*//создание стека
stack* create_stack()
{
	std::cout<<("------------------Создание стека--------------\n");
	stack *top=nullptr;
	int tmp;
	do
	{
		tmp=setint();												//получаем элемент для стека
		push_stack(&top,tmp);										//записываем его в стек

		std::cout<<("Если хотите ввести еще один элемент стека, введите '+'\n");
		fflush(stdin);
	} while(getchar()=='+');
	return top;														//возвращаем вершину стека
}
//создание очереди
void create_q(queue **head, queue **tail)
{
	stack* tmp=nullptr;
	std::cout<<("------------------Создание очереди--------------\n");
	do
	{
		tmp=create_stack();
		push_q(head, tail,tmp);

		std::cout<<("Если хотите ввести еще один элемент очереди, введите '+'\n");
		fflush(stdin);
	} while(getchar()=='+');
}

//сумма элементов в стеке
int sum_stack(stack *list)
{
	int sum=0;
	while(list)
	{
		sum+=list->info;									//прибавляем текущий элемент стека
		list=list->next;									//переход к следующему элементу
	}
	return sum;
}
//поиск минимальной суммы в очереди
queue* find_min_sum(queue* head)
{
	if (!head)
	{
		std::cout<<("Очередь пуста\n");
		return nullptr;
	}
	queue *q_min=head;
	int sum_min=0, sum_now;
	while(head)													//поиск максимальной суммы
	{
		sum_now=sum_stack(head->info);
		if (q_min==head || sum_now<sum_min)
		{
			sum_min=sum_now;
			q_min=head;
		}
		head=head->next;
	}
	std::cout<<("Стек с минимальной суммой:\n");
	see_stack(q_min->info);
	return q_min;
}

//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
void del_min_q(queue **head, queue **tail)
{
	if(!(*head))
	{
		std::cout<<("Очередь пуста\n");
		return;
	}

	queue *for_del=find_min_sum(*head);												//элемент для удаления
	queue *prev_for_del=*head;															//предшествующий элементу для удаления
	stack *stack_end_for_del=for_del->info;												//конец стека элемента для удаления
	queue *for_write=nullptr;																//элемент для записи (куда будем писать стек)

	while (prev_for_del && (prev_for_del->next)!=for_del)								//поиск предыдущего элемента, перед тем, что нужно удалять
	{
		prev_for_del=prev_for_del->next;
	}

	if (for_del->next)														//находим элемент, в который будем писать (доделать)
	{
		for_write=for_del->next;												//пишем в следующий элемент
	}
	else if (prev_for_del)
	{
		for_write=prev_for_del;													//иначе будем писать в предыдущий элемент (если он существует)
	}
	else																		//удаляемый элемент - единственный
	{
		stack *tmp=for_del->info;
		while (tmp)															//удаляем стек
		{
			pop_stack(&tmp);
		}
		free(for_del);
		*head=*tail=nullptr;												//голова и хвост = nullptr
		return;
	}

	if(stack_end_for_del)																				//если стек существует (не пуст), дозапишем его
	{
		while (stack_end_for_del->next)													//проходим до конца удаляемого стека
		{
			stack_end_for_del=stack_end_for_del->next;
		}

		stack_end_for_del->next=for_write->info;											//говорим, что последний элемент удаляемого стека не nullptr, а первый элемент для дозаписи
		for_write->info=for_del->info;												//теперь мы дозаписали стек куда нужно
	}

	if(prev_for_del)																				//если предыдущий элемент есть
	{
		prev_for_del->next=for_del->next;												//перебрасываем указатель на следующий элемент очереди
	}

	if (for_del==(*head))															//если удаляем первый элемент - изменяем голову
	{
		*head=for_del->next;
	}
	if (for_del==(*tail))															//если удаляем последний элемент - изменяем хвост
	{
		*tail=prev_for_del;
	}
	free(for_del);
}

*/