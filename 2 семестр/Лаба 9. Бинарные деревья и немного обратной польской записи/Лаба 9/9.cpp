/*
Сегодня:
	создание бинарного дерева
	загоняем в дерево обратную польскую запись (для упрощения вычисления)
	-узлы=знаки операций
	-листья=символы (числа)

Задача в общем:
	Есть файл
	В нем формулы (несколько выражений)
	Выражение заканчивается =
	Вычитываем его
	перегоняем его в обратную польскую запись
	затем в дерево
	отобразить его
	вычислить выражение из бинарного дерева
	ответ дописать в файл
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>/*
	Создать массив структур
	-Ввод базы данных с клавиатуры (удаление старой БД)
	-Вывод базы на экран
	-Ввод базы данных с клавиатуры (дозапись в старую БД)
	-Поиск по параметру
	-Удаление человека из БД
	-Изменение данных о человеке
*/

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include <locale.h>
#include <string.h>
#define N 30
struct database
{
	char surname[N];		//фамилия
	int god_rogd;			//год рождения
	char type;			//s-студент, r-работник, p-пенсионер
	union						//для каждого типа человека
	{
		struct			//студент
		{
			char VYZ[N];		//ВУЗ
			int group;			//группа
			double sum;			//стипендия
		} s;
		struct			//работник
		{
			char workplace[N];			//предприятие
			int dolzn;			//должность (1,2,3)
			double sum;			//зарплата
		} r;
		struct			//пенсионер
		{
			double sum;				//пенсия
		} p;
	} un;
};

int setyear(database* db=nullptr)					//ввод года рождения с клавиатуры (1900<...<2017)
{
	int tmp;
	do
	{
		printf("Введите год рождения (от 1901 до 2016)\n");
		fflush(stdin);
		scanf("%d", &tmp);
		if (tmp>1900 && tmp<=2016)
		{
			if(db)									//если передан не нулевой указатель, запомнить в БД
			{
				db->god_rogd=tmp;
			}
			return tmp;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

void setdouble(double* to, char* text)					//ввод суммы (стипендия, пенсия, з/п) с клавиатуры
{
	double tmp;
	do
	{
		printf("Введите %s\n",text);
		fflush(stdin);
		scanf("%lf", &tmp);
		if (tmp>=0)										//если такая з/п существует, тогда сохраняем ее, иначе повторный ввод
		{
			*to=tmp;
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

void setstring(char* to, char* text)					//ввод фамилии/университета/места работы с клавиатуры
{
	printf("Введите %s\n", text);
	fflush(stdin);
	fgets(to, N, stdin);
}

void setint(int* to, char* text)					//ввод ID должности/номер группы с клавиатуры
{
	int tmp;
	do
	{
		printf("Введите %s\n", text);
		fflush(stdin);
		scanf("%d", &tmp);
		if (tmp>0)
		{
			*to=tmp;
			return;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

char settype(database* db=nullptr)									//ввод типа человека с клавиатуры
{
	char tmp;
	do
	{
		printf("Введите статус человека (s-студент, r-работник, p-пенсионер)\n");
		fflush(stdin);
		scanf("%c", &tmp);
		if (tmp=='s' || tmp=='r' || tmp=='p')
		{
			if(db)									//если передан не нулевой указатель, запомнить в БД и изменить в ней остальные данные
			{
				db->type=tmp;

				switch( (db)->type)													//изменение остальных данных
				{
					case 's':
					{
						setstring((db)->un.s.VYZ, "ВУЗ");				//ввод ВУЗа
						setint(&((db)->un.s.group), "№ группы");				//ввод № группы
						setdouble(&((db)->un.s.sum), "стипендию");						//ввод стипендии
						break;
					}
					case 'r':
					{
						setstring((db)->un.r.workplace, "место работы");			//ввод места работы
						setint(&((db)->un.r.dolzn), "№ дожности");					//ввод № должности
						setdouble(&((db)->un.r.sum), "зарплату");				//ввод зарплаты
						break;
					}
					case 'p':
					{
						setdouble(&((db)->un.p.sum), "пенсию");				//ввод пенсии
						break;
					}
				}
			}
			return tmp;
		}
		printf("Ошибка. Повторите ввод\n");
	} while (1);
}

database* getdb(int n, database* db=nullptr, int now=0)			//кол-во вводимых структур (новых)
{
	printf("____________________Ввод___________________\n");
	int i;
	db=(database*)realloc(db,(n+now)*sizeof(database));			//выделение памяти под структуру
	if(!db) return db;

	for (i=now; i<(n+now); i++)
	{
		printf("\nВводится человек №%d\n", i+1);

		setstring((db+i)->surname, "фамилию");						//ввод фамилии
		setyear(db+i);						//ввод года рождения
		settype(db+i);						//ввод типа человека

		printf("\nЧеловек №%d введен\n", i+1);
	}

	printf("Вы ввели всех людей\n\n");
	return db;
}

void putdb(database *db, int n)
{
	printf("____________________Вывод___________________\n");
	for (int i=0; i<n; i++)
	{
		printf("----------------Человек №%d--------------------\n", i+1);

		printf("Фамилия человека: %s", (db+i)->surname);			//вывод фамилии
		printf("Год рождения: %d\n", (db+i)->god_rogd);				//вывод года рождения

		switch( (db+i)->type)
		{
			case 's':
			{
				printf("Статус человека: студент\n");			//вывод типа человека
				printf("ВУЗ: %s", (db+i)->un.s.VYZ);			//вывод ВУЗа
				printf("Группа: %d\n", (db+i)->un.s.group);			//вывод № группы
				printf("Стипендия: %lf\n", (db+i)->un.s.sum);			//вывод стипендии

				break;
			}
			case 'r':
			{
				printf("Статус человека: работник\n");			//вывод типа человека
				printf("Место работы: %s",(db+i)->un.r.workplace);			//вывод места работы
				printf("№ должности: %d\n", (db+i)->un.r.dolzn);				//вывод № должности
				printf("Зарплата: %lf\n", (db+i)->un.r.sum);			//вывод зарплаты

				break;
			}
			case 'p':
			{
				printf("Статус человека: пенсионер\n");			//вывод типа человека
				printf("Пенсия: %lf\n", (db+i)->un.p.sum);		//вывод пенсии

				break;
			}
		}
	}
	printf("---------------------Конец--------------------\n\n");
}

void find_name(database* db, int n)							//n - количество элементов структуры
{
	char now[N];										//строка, которую мы будем искать
	int i;
	setstring(now,"фамилию человека, которого хотите найти");

	for(i=0; i<n; i++)
	{
		if (!strcmp(now, (db+i)->surname))						//если найден человек с такой фамилией, вывести его на экран
		{
			putdb((db+i),1);									//вывести на экран этого человека
		}
	}
}

void find_year(database* db, int n)							//n - количество элементов структуры
{
	int i, now;									//для счетчиков//год, который введут
	now=setyear();

	for(i=0; i<n; i++)
	{
		if (now==(db+i)->god_rogd)						//если найден человек с такой фамилией, вывести его на экран
		{
			putdb((db+i),1);									//вывести на экран этого человека
		}
	}
}

void find_type(database* db, int n)							//n - количество элементов структуры
{
	int i;								//для счетчиков
	char now;							//тип, который введут
	now=settype();						//ввод типа человека

	for(i=0; i<n; i++)
	{
		if (now==(db+i)->type)						//если найден человек с такой фамилией, вывести его на экран
		{
			putdb((db+i),1);									//вывести на экран этого человека
		}
	}
}

void del(database* db, int i, int n)
{
	i++;
	for (;i<n; i++)
	{
		*(db+i-1)=*(db+i);
	}
}

void menu_text(int key=0)						//текст меню
{
	if (!key)																//key==0 - основное меню, все остальное - вывод списка параметров (вызывается по умолчанию)
	{
		printf("\nВыберите то, что вы хотите сделать:\n");					//текст меню
		printf("1. Ввод базы данных с клавиатуры (удаление старой БД)\n");
		printf("2. Вывод базы на экран\n");
		printf("3. Ввод базы данных с клавиатуры (дозапись в старую БД)\n");
		printf("4. Поиск по параметру\n");
		printf("5. Удаление человека из БД\n");
		printf("6. Изменение данных о человеке\n");
		printf("0. Выход\n");
	}
	else
	{
		printf("\nВыберите параметр:\n");					//текст меню
		printf("1. Фамилия\n");
		printf("2. Год рождения\n");
		printf("3. Тип\n");
		printf("0. Назад\n");
	}
	printf("Пожалуйста, введите цифру\n");
}

void change(database* hum, int len)
{
	int k, ID;											//для switch//номер структуры

	do
	{
		printf("Введите номер структуры для изменения (начиная с 1)\n");
		scanf("%d", &ID);

		if (ID>0 && ID<=len)
		{
			break;
		}
		printf("Такой структуры не существует. Пожалуйста, повторите ввод\n");								//если такого ID не существует, вывести сообщение об этом
	} while(1);

	ID--;															//в файле нумерация структур с 0

	menu_text(1);														//спрашиваем, что будем делать
	fflush(stdin);
	scanf("%d", &k);										//ввод для switch
	switch(k)
		{
			case 0:										//назад
				{
					return;
				}
			case 1:
				{											//изменение фамилии
					setstring((hum+ID)->surname, "новую фамилию");									//ввод новой фамилии
					break;
				}
			case 2:
				{											//Изменение года рождения
					printf("Новый год рождения:\n");										//ввод новой фамилии
					setyear(hum+ID);
					break;
				}
			case 3:
				{											//Изменение типа
					printf("Новый статус человека:\n");										//ввод новой фамилии
					settype(hum+ID);
					break;
				}
			default:
				{
					printf("Такого пункта меню нет\n");
					break;
				}
		}
}

int menu()
{
	database *db=nullptr;
	int k, n=0;				//для меню//кол-во элементов БД
	do
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					free(db);
					return 0;
				}
			case 1:
				{								//ввод БД с клавиатуры (удаляем старую)
					do
					{
						printf("Введите кол-во элементов базы данных, которые вы хотите ввести\n");
						scanf("%d", &n);
					}while(n<0);

					free(db);					//на всякий случай удаляем то, что там было
					db=getdb(n);				//ввод массива структур
					if (!db)
					{
						printf("Ошибка выделения памяти. Действие не удалось\n");
						n=0;					//обнуление n на случай, если попросят вывод
					}

					break;
				}
			case 2:											//вывод того, что ввели, на экран
				{
					putdb(db,n);
					break;
				}
			case 3:											//Ввод базы данных с клавиатуры (дозапись в старую БД)
				{
					do
					{
						printf("Введите кол-во элементов базы данных, которые вы хотите доввести\n");
						scanf("%d", &k);							//запишем увеличение БД в k
					}while(k<0);

					db=getdb(k, db, n);				//ввод массива структур (k доввести в db, n уже введено)
					n+=k;								//увеличим n (оно теперь n+k)
					if (!db)
					{
						printf("Ошибка выделения памяти. Действие не удалось\n");
						n=0;					//обнуление n на случай, если попросят вывод
					}

					break;
				}
			case 4:											//Поиск по параметру
				{
					menu_text(1);

					fflush(stdin);
					scanf("%d", &k);						//ввод числа для переключения меню

					switch(k)
					{
						case 1:								//поиск по фамилии
							{
								find_name(db,n);
								break;
							}
						case 2:								//поиск по году рождения
							{
								find_year(db,n);
								break;
							}
						case 3:								//поиск по типу
							{
								find_type(db,n);
								break;
							}
						case 0:											//назад
							{
								break;
							}
						default:										//вывод ошибки и назад
							{
								printf("Такого пункта меню нет.\n");
								break;
							}
					}

					break;
				}
			case 5:											//Удаление человека из БД
				{
					printf("Введите номер удаляемого человека\n");
					scanf("%d", &k);										//k теперь хранит параметр для функции

					if(k<=n && k>0)
					{
						k--;											//находим реальный индекс человека
						del(db, k, n);								//удалить из db k-ый элемент (всего там n элементов), нумерация начинается с 0
						n--;
					}
					else
					{
						printf("Человек с таким номером не введен\n");
					}

					break;
				}
			case 6:
				{
					change(db, n);
					break;
				}
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать с базой данных.\n");

	menu();

	printf("\nДля выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}
#include <ctype.h>

struct tree
{
	double info;
	tree* left;
	tree* right;
};
/*
struct stack
{
	int info;
	stack *next;
};

//для работы со стеком
void push_stack(stack **list, int c)
{
	stack *tmp;

	tmp=(stack*)malloc(1*sizeof(stack));								//выделение памяти
	if (!tmp) return;

	tmp->info=c;														//заполнение полей элемента
	tmp->next=*list;

	*list=tmp;															//изменение верхушки стека
}
int pop_stack(stack **list)
{
	stack *tmp=*list;
	if(!(*list)) return 0;
	int c=(*list)->info;																//то, что хранится внутри

	*list=(*list)->next;															//изменение верхушки стека

	free(tmp);																		//удаление старой верхушки
	return c;
}
*/
//функция ввода строки
char* set_obratnaya_polskaya_zapis()
{
	int n, i;												//длина слова//для цикла
	char* s;											//вводимое слово

	do
	{
		printf("Введите длину строки\n");
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите число\n");
	} while(1);

	n++;
	s=(char*)malloc(n*sizeof(char));
	if (!s) return nullptr;

	printf("Введите обратную польскую запись\n");
	fflush(stdin);
	fgets(s,n,stdin);

	for(i=0; s[i] && s[i]!='\n'; i++);							//теперь в конце гарантированно \0
	s[i]='\0';

	return s;
}

//для работы с деревом (не сделано)
tree* create_new_tree_element(double info_to_add)										//добавление элемента дерева
{
	tree *tmp;

	tmp=(tree*)malloc(1*sizeof(tree));								//выделение памяти
	if (!tmp) return tmp;

	tmp->info=info_to_add;														//заполнение полей элемента
	tmp->left=tmp->right=nullptr;

	return tmp;
}
//просмотр дерева
void see_tree(tree *root)
{
	if(!root)
	{
		//printf("Дерево пусто\n");
		return;
	}

	//printf("---------------То, что в дереве-------------\n");
	see_tree (root->left);
	see_tree (root->right);
	printf("%lf", root->info);
	//printf("----------------Конец--------------\n");
	return;
}

//записываем в дерево ОПЗ
void add_string_to_tree(char* s, tree **root)
{
	static int i = - 1;								//что именно нужно записать

	if (i<0)
	{
		i = strlen(s) - 1;
	}

	*root=create_new_tree_element(s[i]);
	i--;
	if (isalpha(s[i+1]) )													//если введен символ - выход
	{
		return;
	}

	add_string_to_tree(s, &((*root)->right) );
	add_string_to_tree(s, &((*root)->left) );
}

//проверяем обратную польскую запись на правильность
int check_vaild(char *s)
{
	int i;										//для обхода по строке
	int k=0;												//сколько чисел на данный момент должно быть в стеке

	for (i=0; s[i]; i++)								//проход по строке
	{
		if (isalpha(s[i]))												//введене буква
		{
			k++;
			continue;
		}
																			//введен знак
		k--;																//со стека забирается 2 символа и добавляется 1 (результат операции) - итого отнимает одно число

		if (k<1)															//значит в стеке не хватило элементов
		{
			return 0;
		}
	}

	if (k!=1)																//в стеке должен остаться 1 элемент - ответ
	{
		return 0;
	}

	return 1;
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая заносит обратную польскую запись в бинарное дерево.\n");

	char* s;
	tree* root=nullptr;
	printf("Введите обратную польскую запись.\n");
	s=set_obratnaya_polskaya_zapis();

	if (!check_vaild(s))
	{
		printf("\nОбратная польская запись некорректна. Программа завершила свою работу. Для выхода введите любой символ. До свидания!\n");
		fflush(stdin);
		getch();
		return 0;
	}

	add_string_to_tree(s, &root);

	see_tree(root);

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}