/*
	Создание бинарного файла (набор struct)
	с информацией о людях (фамилия, год)
	В файле:
	-найти и вывести информацию
	-изменить
	-удалить
	-добавить (в фиксированную позицию)
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include <io.h>
#include <string.h>
#define N 30									//длина строки

struct people
{
	char name[N];						//фамилия
	int year;							//год рождения
};

//функция ввода числа
int setint()
{
	int n;												//вводимое число

	printf("Введите год рождения человека\n");
	do
	{
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите целое число\n");
	} while(1);

	return n;
}

//функция ввода строки
void setst(char s[N])
{
	int i;												//для цикла

	printf("Введите фамилию\n");
	fflush(stdin);
	fgets(s,N,stdin);

	for(i=0; s[i] && s[i]!='\n'; i++);							//теперь в конце гарантированно \0
	s[i]='\0';
}

//функция ввода структуры в массив
void setpeople(people* hum, int n=1)								//n - кол-во вводимых структур
{
	int i;				//для цикла
	for(i=0; i<n; i++)
	{
		setst((hum+i)->name);									//ввод имени
		(hum+i)->year=setint();									//ввод года рождения
	}
}

//функции ввода структуры в файл
void fputpeople(char* adress, people* st, int n=1)						//сохраняет в файл с адресом adress структуру st
{
	FILE* f;
	f=fopen(adress, "ab");
	fwrite(st, sizeof(people),n,f);
	fclose(f);
}

void putf(char* file_adress, const char param[])			//оболочка для fputs
{
	FILE* f=fopen(file_adress,param);								//если захотят обнулить файл - сделаем это
	fclose (f);

	people hum;

	do
	{
		printf("Ввод структуры\n");
		setpeople(&hum);						//чтение 1 структуры с клавиатуры
		fputpeople(file_adress, &hum);					//запись структуры в файл
		printf("Если хотите ввести еще одну структуру, введите 'Y'\n");
		fflush(stdin);
	} while (getchar()=='Y');

	printf("Ввод завершен\n");
}

//вывод структуры на экран
void printpeople(people* hum, int n=1)
{
	int i;						//для цикла
	for(i=0; i<n;i++)
	{
		printf("\n------------------------Человек---------------------------\n");
		printf("Фамилия: ");
		puts((hum+i)->name);
		printf("Год рождения: %d\n", (hum+i)->year);
	}
}

//функция вывода на экран содержимого в файле
void fprint(char* adress)						//читает содержимое файла
{
	FILE* f;
	people tmp;										//tmp - текущая структура

	printf("Сейчас в файле:\n");

	f=fopen(adress, "rb");
	while(1)
	{
		fread(&tmp, sizeof(people),1,f);							//чтение из файла по символу
		if (feof(f)) break;
		printpeople(&tmp);											//выводим на экран 1 человека
	}
	fclose(f);
}

//функция поиска
fpos_t find(char* adress, const char name[N], int n=1)					//name - искомая фамилия//n - № искомой структуры//возвращает позицию этой структуры
{
	fpos_t pos;									//позиция начала этой структуры
	FILE* f=fopen(adress, "rb");						//открываем файл
	people tmp;											//то, что прочитали сейчас


	while (!feof(f) && n>0)
	{
		fgetpos(f, &pos);								//запомним позицию начала числа
		fread(&tmp, sizeof(people),1,f);									//сканируем текущее число
		if(!strcmp(tmp.name, name)) n--;						//если нашли такое-же число - уменьшаем счетчик
	}

	fclose(f);

	if (n<=0)												//если найдено число, вернем позицию
	{
		return pos;
	}
	return -1;														//иначе вернем -1 (число не найдено)
}
//оформление для find
void poisk_name(char ad[])
{
	fpos_t pos;											//тут будут храниться позиция начала найденного числа
	char s[N];

	printf("Фамилия, которую хотите найти:\n");						//установим фамилию для поиска
	setst(s);

	pos=find(ad,s);													//запустим поиск

	if(pos==-1)													//если число не найдено, вывод уведомления об этом, иначе вывод его позиции
	{
		printf("Такая фамилия не найдена\n");
	}
	else
	{
		printf("Это %d-ая структура", ((int)pos+1)/sizeof(people)+1);					//вывод результирующей информации
	}
}

//функции удаления участка файла
void fdel(char adress[], fpos_t begin, fpos_t end)
{
	if (begin>=end) return;										//если начало дальше конца - то выйти
	FILE* f=fopen(adress,"r+b");								//открываем файл для изменения
	people tmp;													//в нее будем писать по символу

	for(; ; begin+=sizeof(people), end+=sizeof(people))							//цикл переброса участка файла
	{
		fsetpos(f, &end);										//ставим УТПФ на структуру, которое будем забирать
		fread(&tmp, sizeof(people), 1, f);									//забираем структуру
		if (feof(f)) break;										//если достигли конца файла - выход
		fsetpos(f, &begin);										//ставим УТПФ на структуру, в которое будем писать
		fwrite(&tmp, sizeof(people), 1, f);									//пишем структуру
	}

	chsize(fileno(f), begin);

	fclose(f);													//закрытие файла
}
//оформление для fdel
void delpeople(char ad[])					//оболочка для fdel
{
	int begin;								//первый удаляемый символ//первый неудаляемый символ (после begin)

	do																			//ввод первой удаляемой позиции
	{
		printf("Введите № структуры, которую хотите удалить (нумерация начинается с 1)\n");
		scanf("%d", &begin);
		if (begin>0) break;											//если введена существующая позиция, то выходим из цикла
		printf("Ошибка, повторите ввод\n");
	} while (1);

	begin--;															//нумерация в массиве ведь с нуля
	fdel(ad, (fpos_t)(begin*sizeof(people)), (fpos_t)((begin+1)*sizeof(people)));
}

//текст меню
void menu_text(int key=0)						//текст меню
{
	if (key==1)
	{
		printf("\nВыберите то, что вы хотите изменить:\n");					//текст меню
		printf("1. Фамилию\n");
		printf("2. Год рождения\n");
		printf("0. Назад\n");
		printf("Пожалуйста, введите цифру\n");
		return;
	}
	printf("\nВыберите то, что вы хотите сделать:\n");					//текст меню
	printf("1. Ввести массив структур (с перезаписью)\n");
	printf("2. Вывести файл на экран\n");
	printf("3. Найти человека по фамилии\n");
	printf("4. Изменить человека\n");
	printf("5. Удалить человека\n");
	printf("6. Добавить человека\n");
	printf("0. Выход\n");
	printf("Пожалуйста, введите цифру\n");
}


//изменение структуры
void change(const char ad[])
{
	FILE* f=fopen(ad,"r+b");						//открытие файла
	people hum;										//изменяемая структура
	fpos_t p;												//позиция этой структуры в файле
	int k, ID;											//для switch//номер структуры

	do
	{
		printf("Введите номер структуры (начиная с 1)\n");
		scanf("%d", &ID);

		if (ID>0 && ID<=(filelength(fileno(f))/sizeof(people)))
		{
			break;
		}
		printf("Такой структуры не существует. Пожалуйста, повторите ввод\n");								//если такого ID не существует, вывести сообщение об этом
	} while(1);

	ID--;															//в файле нумерация структур с 0

	p=(fpos_t)(ID*sizeof(people));

	fsetpos(f, &p);											//установить УТПФ на нужную структуру
	fread(&hum,sizeof(people),1,f);														//прочитать структуру, которую хотим изменить

	menu_text(1);
	fflush(stdin);
	scanf("%d", &k);										//ввод для switch
	switch(k)
		{
			case 0:										//назад
				{
					return;
				}
			case 1:
				{											//изменение фамилии
					printf("Новая фамилия:\n");										//ввод новой фамилии
					setst(hum.name);
					break;
				}
			case 2:
				{											//Изменение года рождения
					printf("Новый год рождения:\n");										//ввод нового года рождения
					hum.year=setint();
					break;
				}
			default:
				{
					printf("Такого пункта меню нет\n");
					break;
				}
		}

	fsetpos(f,&p);																			//установка УТПФ на место для записи
	fwrite(&hum,sizeof(people),1,f);														//записать измененную структуту

	fclose(f);
}

//добавление структуры на определенную позицию (механизм)
int fputpeople_fix_pos(char* adress, people* st, int pos_num)						//сохраняет в файл с адресом adress структуру st. st станет на позицию pos_num (нумерация с 0, 1 структура - 1 позиция)
{
	FILE* f=fopen(adress, "r+b");
	fpos_t begin, end;															//начало и конец для перезаписи

	if(pos_num> filelength(fileno(f))/sizeof(people))
	{
		printf("Ошибка. Введена неверная позиция. Ввод прекращен. Структура не записана\n");
		return 1;
	}

	fseek(f,0,2);																	//ставим УТПФ в конец
	fgetpos(f,&end);																//читаем end (где конец файла)
	begin=end-sizeof(people);														//устанавливаем на элемент для сдвига

	for(; begin>=(sizeof(people)*pos_num); begin-=sizeof(people), end-=sizeof(people))							//цикл переброса участка файла
	{
		people tmp;												//временная структура для сдвига
		fsetpos(f, &begin);										//ставим УТПФ на структуру, которое будем забирать
		fread(&tmp, sizeof(people), 1, f);									//забираем структуру
		fsetpos(f, &end);										//ставим УТПФ на структуру, в которое будем писать
		fwrite(&tmp, sizeof(people), 1, f);									//пишем структуру
	}

	fseek(f,pos_num*sizeof(people), 0);								//ставим УТПФ на позицию для записи
	fwrite(st, sizeof(people),1,f);										//пишем структуру
	fclose(f);
	return 0;
}
//оболочка для fputpeople_fix_pos
void add_to_fix_pos(char* file_adress)			//оболочка для fputpeople_fix_pos
{
	people hum;
	int pos;

	printf("Введите позицию новой структуры (с 0)\n");														//ввод, какой станет введенная структура
	scanf("%d", &pos);

	do
	{
		printf("Ввод структуры\n");
		setpeople(&hum);						//чтение 1 структуры с клавиатуры
		if(fputpeople_fix_pos(file_adress, &hum, pos))					//запись структуры в файл на определенную позицию
			break;																	//если вернули 1 (структура не введена), выход
		pos++;														//если захотят ввести еще 1 структуру
		printf("Если хотите ввести еще одну структуру, введите 'Y'\n");
		fflush(stdin);
	} while (getchar()=='Y');

	printf("Ввод завершен\n");
}

//функция меню (-)
int menu()
{
	char file_adress[]="bin.dat";								//адрес файла
	int k;				//для меню
	do
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					return 0;
				}
			case 1:
				{											//Ввести массив структур (с перезаписью)
					putf(file_adress,"wb");
					break;
				}
			case 2:
				{											//Вывод содержимого файла
					fprint(file_adress);
					break;
				}
			case 3:
				{											//Найти структуру по фамилии
					poisk_name(file_adress);
					break;
				}
			case 4:
				{											//изменить структуру
					change(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 5:											//удалить структуру
				{
					delpeople(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 6:											//добавить структуру
				{
					add_to_fix_pos(file_adress);
					break;
				}
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}


int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать с бинарными файлами.\n");

	menu();

	printf("\nДля выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}





/*
//функции удаления участка файла
void fdel(char adress[], fpos_t begin, fpos_t end)
{
	if (begin>=end) return;										//если начало дальше конца - то выйти
	FILE* f=fopen(adress,"r+b");								//открываем файл для изменения
	int tmp;													//в нее будем писать по символу

	for(; ; begin+=4, end+=4)							//цикл переброса участка файла
	{
		fsetpos(f, &end);										//ставим УТПФ на число, которое будем забирать
		fread(&tmp, sizeof(int), 1, f);									//забираем число
		if (feof(f)) break;										//если достигли конца файла - выход
		fsetpos(f, &begin);										//ставим УТПФ на число, в которое будем писать
		fwrite(&tmp, sizeof(int), 1, f);									//пишем число
	}

	chsize(fileno(f), begin);

	fclose(f);													//закрытие файла
}
//оформление для fdel
void delf(char ad[])					//оболочка для fdel
{
	int begin, end;								//первый удаляемый символ//первый неудаляемый символ (после begin)

	do																			//ввод первой удаляемой позиции
	{
		printf("Введите № числа, которое хотите удалить (нумерация начинается с 0)\n");
		scanf("%d", &begin);
		if (begin>=0) break;											//если введена существующая позиция, то выходим из цикла
		printf("Ошибка, повторите ввод\n");				//ввод первой удаляемой позиции
	}while (1);

	do																			//ввод первой неудаляемой позиции после begin
	{
		printf("Введите № числа, которое хотите оставить\n");
		scanf("%d", &end);
		if (end>=begin) break;											//если введена существующая позиция, то выходим из цикла
		printf("Ошибка, повторите ввод\n");				//ввод первой удаляемой позиции
	}while (1);

	fdel(ad, (fpos_t)(begin*4), (fpos_t)(end*4));
}

//поиск и удаление
void find_del(char ad[])
{
	fpos_t pos;											//тут будут храниться позиция начала найденного числа
	int a;

	printf("Число, которое хотите удалить:\n");						//установим слово для поиска
	a=setint();

	pos=find(ad,a);													//запустим поиск

	if(pos==-1)													//если число не найдено, вывод уведомления об этом, иначе вывод его позиции
	{
		printf("Такое число не найдено. Удаление не будет произведено\n");
		return;
	}

	fdel(ad, (fpos_t)pos, (fpos_t)(pos+sizeof(int)));						//удаление
}
*/