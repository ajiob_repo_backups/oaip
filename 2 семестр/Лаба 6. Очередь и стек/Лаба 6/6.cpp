/*
	Дана очередь стеков чисел
	-создать очередь стеков и просмотреть ее
	-найти min стек в очереди по сумме (вывести его на экран)
	-удалить его, стек перезаписать на вершину след элем. очереди
		(если это последний элемент, записать его в предыдущий)
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <string.h>

struct stack
{
	int info;
	stack *next;
};
struct queue
{
	stack* info;
	queue* next;
};

int setint()
{
	int n;												//вводимое число

	printf("Введите число\n");
	do
	{
		if(scanf("%d", &n))
		{
			break;
		}
		fflush(stdin);
		printf("Ошибка! Введите целое число\n");
	} while(1);

	return n;
}

//для работы со стеком
void push_stack(stack **list, int to_add)
{
	stack *tmp;

	tmp=(stack*)malloc(1*sizeof(stack));								//выделение памяти
	if (!tmp) return;

	tmp->info=to_add;														//заполнение полей элемента
	tmp->next=*list;

	*list=tmp;															//изменение верхушки стека
}
int pop_stack(stack **list)
{
	stack *tmp=*list;
	if(!(*list)) return 0;
	char c=(*list)->info;																//то, что хранится внутри

	*list=(*list)->next;															//изменение верхушки стека

	free(tmp);																		//удаление старой верхушки
	return c;
}
int see_stack_top(stack *list)
{
	if(!list) return 0;
	return list->info;
}
void see_stack(stack *list)
{
	if(!list)
	{
		printf("Стек пуст\n");
		return;
	}

	printf("---------------То, что в стеке-------------\n");
	while(list)
	{
		printf("%d\n", list->info);
		list=list->next;
	}
	printf("----------------Конец--------------\n");
	return;
}

//для работы с очередью
void push_q(queue **head, queue **tail, stack* info_to_add)										//добавление элемента очереди
{
	queue *tmp;

	tmp=(queue*)malloc(1*sizeof(queue));								//выделение памяти
	if (!tmp) return;

	tmp->info=info_to_add;														//заполнение полей элемента
	tmp->next=nullptr;

	if(*head)															//если очередь пуста, нужно записать и голову, и хвост
	{
		(*tail)->next=tmp;
		*tail=tmp;
		return;
	}

	*head=*tail=tmp;															//изменение хвоста очереди
}
stack* pop_q(queue **head, queue **tail)
{
	if(!(*head)) return nullptr;
	queue *tmp=*head;
	stack* to_return=(*head)->info;																//то, что хранится внутри

	*head=(*head)->next;															//изменение головы очереди

	if(!(*head))																	//если удален единственный элемент, тогда хвост указывает на nullptr
	{
		*tail=nullptr;
	}

	free(tmp);																		//удаление старой головы очереди
	return to_return;
}
stack* see_q_top(queue *head)
{
	if(!head) return 0;
	return head->info;
}
void see_q(queue *head)
{
	if(!head)
	{
		printf("Очередь пуста\n");
		return;
	}

	printf("---------------То, что в очереди-------------\n");
	while(head)
	{
		see_stack(head->info);
		head=head->next;
	}
	printf("----------------Конец--------------\n");
	return;
}

//создание стека
stack* create_stack()
{
	printf("------------------Создание стека--------------\n");
	stack *top=nullptr;
	int tmp;
	do
	{
		tmp=setint();												//получаем элемент для стека
		push_stack(&top,tmp);										//записываем его в стек

		printf("Если хотите ввести еще один элемент стека, введите '+'\n");
		fflush(stdin);
	} while(getchar()=='+');
	return top;														//возвращаем вершину стека
}
//создание очереди
void create_q(queue **head, queue **tail)
{
	stack* tmp=nullptr;
	printf("------------------Создание очереди--------------\n");
	do
	{
		tmp=create_stack();
		push_q(head, tail,tmp);

		printf("Если хотите ввести еще один элемент очереди, введите '+'\n");
		fflush(stdin);
	} while(getchar()=='+');
}

//сумма элементов в стеке
int sum_stack(stack *list)
{
	int sum=0;
	while(list)
	{
		sum+=list->info;									//прибавляем текущий элемент стека
		list=list->next;									//переход к следующему элементу
	}
	return sum;
}
//поиск минимальной суммы в очереди
queue* find_min_sum(queue* head)
{
	if (!head)
	{
		printf("Очередь пуста\n");
		return nullptr;
	}
	queue *q_min=head;
	int sum_min=0, sum_now;
	while(head)													//поиск максимальной суммы
	{
		sum_now=sum_stack(head->info);
		if (q_min==head || sum_now<sum_min)
		{
			sum_min=sum_now;
			q_min=head;
		}
		head=head->next;
	}
	printf("Стек с минимальной суммой:\n");
	see_stack(q_min->info);
	return q_min;
}

//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
void del_min_q(queue **head, queue **tail)
{
	if(!(*head))
	{
		printf("Очередь пуста\n");
		return;
	}

	queue *for_del=find_min_sum(*head);												//элемент для удаления
	queue *prev_for_del=*head;															//предшествующий элементу для удаления
	stack *stack_end_for_del=for_del->info;												//конец стека элемента для удаления
	queue *for_write=nullptr;																//элемент для записи (куда будем писать стек)

	while (prev_for_del && (prev_for_del->next)!=for_del)								//поиск предыдущего элемента, перед тем, что нужно удалять
	{
		prev_for_del=prev_for_del->next;
	}

	if (for_del->next)														//находим элемент, в который будем писать (доделать)
	{
		for_write=for_del->next;												//пишем в следующий элемент
	}
	else if (prev_for_del)
	{
		for_write=prev_for_del;													//иначе будем писать в предыдущий элемент (если он существует)
	}
	else																		//удаляемый элемент - единственный
	{
		stack *tmp=for_del->info;
		while (tmp)															//удаляем стек
		{
			pop_stack(&tmp);
		}
		free(for_del);
		*head=*tail=nullptr;												//голова и хвост = nullptr
		return;
	}

	if(stack_end_for_del)																				//если стек существует (не пуст), дозапишем его
	{
		while (stack_end_for_del->next)													//проходим до конца удаляемого стека
		{
			stack_end_for_del=stack_end_for_del->next;
		}

		stack_end_for_del->next=for_write->info;											//говорим, что последний элемент удаляемого стека не nullptr, а первый элемент для дозаписи
		for_write->info=for_del->info;												//теперь мы дозаписали стек куда нужно
	}

	if(prev_for_del)																				//если предыдущий элемент есть
	{
		prev_for_del->next=for_del->next;												//перебрасываем указатель на следующий элемент очереди
	}

	if (for_del==(*head))															//если удаляем первый элемент - изменяем голову
	{
		*head=for_del->next;
	}
	if (for_del==(*tail))															//если удаляем последний элемент - изменяем хвост
	{
		*tail=prev_for_del;
	}
	free(for_del);
}

//текст меню
void menu_text(int key=0)						//текст меню
{
	/*
	if (key==1)
	{
		printf("\nВыберите то, что вы хотите изменить:\n");					//текст меню
		printf("1. Фамилию\n");
		printf("2. Год рождения\n");
		printf("0. Назад\n");
		printf("Пожалуйста, введите цифру\n");
		return;
	}*/
	printf("\nВыберите то, что вы хотите сделать:\n");					//текст меню
	printf("1. Создать очередь стеков\n");
	printf("2. Вывести очередь на экран\n");
	printf("3. Найти минимальный стек в очереди по сумме\n");
	printf("4. Удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину стека следующего элемента очереди\n");
	/*
	printf("5. Удалить человека\n");
	printf("6. Добавить человека\n");
	*/
	printf("0. Выход\n");
	printf("Пожалуйста, введите цифру\n");
}
//само меню
int menu()
{
	queue *head=nullptr, *tail=nullptr;											//голова и хвост очереди
	int k;				//для меню
	do
	{
		menu_text();

		fflush(stdin);
		scanf("%d", &k);												//ввод числа для переключения меню

		switch(k)
		{
			case 0:										//выход
				{
					return 0;
				}
			case 1:
				{											//Создать очередь стеков
					create_q(&head, &tail);
					break;
				}
			case 2:
				{											//Вывести очередь на экран
					see_q(head);
					break;
				}
			case 3:
				{											//Найти минимальный стек в очереди по сумме
					find_min_sum(head);
					break;
				}
			case 4:
				{											//удалить элемент очереди, содержащий минимальный стек, а стек дозаписать на вершину след. элемента очереди
					del_min_q(&head, &tail);
					printf("\n");
					see_q(head);							//простотр того, что в стеке
					break;
				}/*
			case 5:											//удалить структуру
				{
					delpeople(file_adress);
					fprint(file_adress);						//то, что сейчас в файле
					break;
				}
			case 6:											//добавить структуру
				{
					add_to_fix_pos(file_adress);
					break;
				}*/
			default:
				{
					printf("Такого пункта меню нет. Пожалуйста, повторите ввод\n");
					break;
				}
		};
	} while(1);
}

int main()
{
	system("chcp 65001");
	printf("Здравствуйте! Вы запустили программу, которая позволяет работать с очередью стеков.\n");

	menu();

	printf("\nПрограмма завершила свою работу. Для выхода введите любой символ. До свидания!\n");
	fflush(stdin);
	getch();
	return 0;
}
